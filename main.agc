
// Project: esphoto 
// Created: 2016-02-28 by Afoni Vladimir (stormarchitextor@gmail.com)

//### CONSTANTS ###//
#constant SCREEN_WIDTH 1024
#constant SCREEN_HEIGHT 600

#constant PLAY_REPEAT 1

#constant UI_ZOOM_IN 2000
#constant UI_ZOOM_OUT 2001

#constant UI_DEPTH 50

#constant MAX_OUTFIT_PER_POSE 5

#constant OUTFIT_BARE 1
#constant OUTFIT_DRESS 2
#constant OUTFIT_PIONEER 3
#constant OUTFIT_SPORT 4
#constant OUTFIT_SWIM 5


#constant MAX_SLAVYA_POSES 4

#constant SLAVYA_POSE_1 1
#constant SLAVYA_POSE_2 2
#constant SLAVYA_POSE_3 3
#constant SLAVYA_POSE_4 4

#constant OUTFIT_UI_POS_X = 5
#constant OUTFIT_UI_POS_Y = 100

#constant OUTFIT_UI_SHIFT_X = 130
#constant OUTFIT_UI_SHIFT_Y = 270

#constant OUTFIT_UI_SCALE_X = 1.0
#constant OUTFIT_UI_SCALE_Y = 1.0

#constant OUTFIT_UI_SCALE2_X = 0.6
#constant OUTFIT_UI_SCALE2_Y = 0.6

#constant OUTFIT_P1_UI_BARE_IMG 3000
#constant OUTFIT_P1_UI_DRESS_IMG 3001
#constant OUTFIT_P1_UI_PIONEER_IMG 3002
#constant OUTFIT_P1_UI_SPORT_IMG 3003
#constant OUTFIT_P1_UI_SWIM_IMG 3004

#constant SL_P1_EMOTION_NORMAL 3100
#constant SL_P1_EMOTION_SERIOUS 3101
#constant SL_P1_EMOTION_SMILE 3102

#constant BACKGROUND_BEACH_SUNSET 1000
#constant BACKGROUND_DEPTH 10000
#constant BACKGROUND_SCALE_X = 0.55
#constant BACKGROUND_SCALE_Y = 0.55

#constant GIRL_BODY_DEPTH 200
#constant GIRL_OUTFIT_DEPTH 190
#constant GIRL_EMOTION_DEPTH 180

#constant SLAVYA_BODY_1 100

#constant SLAVYA_EM_NORMAL 200
#constant SLAVYA_EM_SMILE 201

#constant slavya_outfit_dress_DRESS_1 300
#constant SLAVYA_OUTFIT_SWIM_1 301
#constant SLAVYA_OUTFIT_PIONEER_1 302
#constant SLAVYA_OUTFIT_SPORT_1 303

#constant start_scale_x# = 0.2
#constant start_scale_y# = 0.2

#constant EMOTIONS_SHIFT_X = 53
#constant EMOTIONS_SHIFT_Y = 39

#constant OUTFIT_SHIFT_X = 18
#constant OUTFIT_SHIFT_Y = 87

#constant SWIM_SHIFT_X = 13
#constant SWIM_SHIFT_Y = 5

#constant PIONEER_SHIFT_X = -1
#constant PIONEER_SHIFT_Y = -5

#constant SPORT_SHIFT_X = 10
#constant SPORT_SHIFT_Y = 5

#constant LOADING_TEXT_ID 5
#constant LOADING_STAGE_TEXT_ID 6

#constant POSE_ACTIVE 1
#constant POSE_INACTIVE 0

#constant MAX_EMOTIONS 10

#constant EMOTION_UI_POS_X 10
#constant EMOTION_UI_POS_Y 10

#constant EMOTION_UI_SCALE_X 0.5
#constant EMOTION_UI_SCALE_Y 0.5

#constant EM_NORMAL_BUTTON 1000

#constant BUTTON_NORMAL_IMAGE 2100

//### TYPES ###//
Type Point
	X#
	Y#
EndType

Type UIButton
	pos as Point
	scale as Point
	sprite as Integer
	id as integer
EndType

Type PoseOutfitUI
	bareButton as UIButton 
	dressButton as UIButton
	pioneerButton as UIButton
	sportButton as UIButton
	swimButton as UIButton
EndType

Type ModelEmotionUI
	caption as string
	emotionButton as UIButton
	backButton as UIButton
EndType

Type ModelPose
	id as integer
	lastEmotion as integer
	outfitUI as PoseOutfitUI
	emotionUI as ModelEmotionUI[ MAX_EMOTIONS ]
	active as integer //0 - inactive
EndType


//*****************//
//&&& FUNCTIONS &&&//
//*****************//

//### GIRL MANAGEMENT FUNCTION ###//

Function IsGirlSpriteHit( sprite_hit )
	res = slavya_body  
	res = res or (sprite_hit = slavya_emotions) 
	res = res or (sprite_hit = slavya_outfit_dress)
	res = res or (sprite_hit = slavya_outfit_swim)
EndFunction	res



Function SetSlavyaPosition( x, y, sx#, sy# )
	SetSpritePosition( slavya_body, x, y )
	SetSpritePosition( slavya_emotions, x + (EMOTIONS_SHIFT_X)*sx#/start_scale_x#, y + (EMOTIONS_SHIFT_Y)*sy#/start_scale_y# )
	SetSpritePosition( slavya_outfit_dress, x + (OUTFIT_SHIFT_X)*sx#/start_scale_x#, y + (OUTFIT_SHIFT_Y)*sy#/start_scale_y# )
	SetSpritePosition( slavya_outfit_swim, x + (OUTFIT_SHIFT_X + SWIM_SHIFT_X)*sx#/start_scale_x#, y + (OUTFIT_SHIFT_Y + SWIM_SHIFT_Y)*sy#/start_scale_y# )
	SetSpritePosition( slavya_outfit_pioneer, x + (OUTFIT_SHIFT_X + PIONEER_SHIFT_X)*sx#/start_scale_x#, y + (OUTFIT_SHIFT_Y + PIONEER_SHIFT_Y)*sy#/start_scale_y# )
	SetSpritePosition( slavya_outfit_sport, x + (OUTFIT_SHIFT_X + SPORT_SHIFT_X)*sx#/start_scale_x#, y + (OUTFIT_SHIFT_Y + SPORT_SHIFT_Y)*sy#/start_scale_y# )
EndFunction

Function ZoomSlavya( sx#, sy# )
	SetSpriteScale( slavya_body, sx#, sy# )
	SetSpriteScale( slavya_emotions, sx#, sy# )
	SetSpriteScale( slavya_outfit_dress, sx#, sy# )
	SetSpriteScale( slavya_outfit_swim, sx#, sy# )
	SetSpriteScale( slavya_outfit_pioneer, sx#, sy# )
	SetSpriteScale( slavya_outfit_sport, sx#, sy# )
EndFunction


Function HighlightZoomButtons( ui_hit )
	zoom_in_button_hit = FALSE
	zoom_out_button_hit = FALSE
	
	if( ui_hit = zoom_in_button )
		SetSpriteColorBlue( zoom_in_button, 23 )			
		SetSpriteColorBlue( zoom_out_button, 255 )
		zoom_in_button_hit = TRUE
	elseif( ui_hit = zoom_out_button )
		SetSpriteColorBlue( zoom_out_button, 23 )			
		SetSpriteColorBlue( zoom_in_button, 255 )
		zoom_out_button_hit = TRUE
	else
		SetSpriteColorBlue( zoom_in_button, 255 )
		SetSpriteColorBlue( zoom_out_button, 255 )
		old_ui_sprite = 0
	endif
	
	if( GetPointerState() = 1 ) // display pressed state
		if( ui_hit = zoom_in_button )
			SetSpriteColorRed( zoom_in_button, 75 )
			model_scale_x# = model_scale_x# + 0.01
			model_scale_y# = model_scale_y# + 0.01
			
			SetSlavyaPosition( body_offset_x, body_offset_y, model_scale_x#, model_scale_y# )
			ZoomSlavya( model_scale_x#, model_scale_y#)

		elseif( ui_hit = zoom_out_button )
			SetSpriteColorRed( zoom_out_button, 75 )
			model_scale_x# = model_scale_x# - 0.01
			model_scale_y# = model_scale_y# - 0.01
			SetSlavyaPosition( body_offset_x, body_offset_y, model_scale_x#, model_scale_y# )
			ZoomSlavya( model_scale_x#, model_scale_y#)
		endif
	endif	
EndFunction

Function HighlightButton(ui_hit, button as UIButton)
	if( ui_hit = button.sprite )
		SetSpriteColorBlue( button.sprite, 50 )
	else
		SetSpriteColorBlue( button.sprite, 255 )
	endif
EndFunction

Function HighlightBareButton(ui_hit)
	HighlightButton( ui_hit, activePose.outfitUI.bareButton )
EndFunction

Function HighlightDressButton(ui_hit)
	HighlightButton( ui_hit, activePose.outfitUI.dressButton )
EndFunction

Function HighlightSwimButton(ui_hit)
	HighlightButton( ui_hit, activePose.outfitUI.swimButton )
EndFunction

Function HighlightPioneerButton(ui_hit)
	HighlightButton( ui_hit, activePose.outfitUI.pioneerButton )
EndFunction

Function HighlightSportButton(ui_hit)
	HighlightButton( ui_hit, activePose.outfitUI.sportButton )
EndFunction

//#### UI FUNCTIONS #####//
Function IsZoomButtonHit()
	res = zoom_in_button_hit or zoom_out_button_hit
EndFunction res

Function SetupButton(  button_id, sprite_id, pos_x#, pos_y#, scale_x#, scale_y# )
	button as UIButton
	button.id = button_id
	button.sprite = CreateSprite( sprite_id )
	button.pos.X# = pos_x#
	button.pos.Y# = pos_y#
	button.scale.X# = scale_x#
	button.scale.Y# = scale_y#	
EndFunction button

Function SetupOutfitSpriteButton( sprite_id, pos_x#, pos_y# )
	button as UIButton
	button = SetupButton( sprite_id, sprite_id, pos_x#, pos_y#, OUTFIT_UI_SCALE_X, OUTFIT_UI_SCALE_Y )
EndFunction button

Function SetupEmotionSpriteButton( sprite_id, pos_x#, pos_y# )
	button as UIButton
	button = SetupButton( sprite_id, sprite_id, pos_x#, pos_y#, EMOTION_UI_SCALE_X, EMOTION_UI_SCALE_Y )
EndFunction button

Function SetupSmallOutfitSpriteButton( sprite_id, pos_x#, pos_y# )
	button as UIButton
	button = SetupButton( sprite_id, sprite_id, pos_x#, pos_y#, OUTFIT_UI_SCALE2_X, OUTFIT_UI_SCALE2_Y )
EndFunction button


Function SetUISpriteButton( button as UIButton )
	SetSpriteDepth( button.sprite, UI_DEPTH )
	SetSpritePosition( button.sprite, button.pos.x#, button.pos.y# )
	SetSpriteScale( button.sprite, button.scale.X#, button.scale.Y# )
EndFunction


Function ShowLoadStage( message as string )
	SetTextString(LOADING_STAGE_TEXT_ID, message)
	Sync()
EndFunction


//********************//
//&& INITIALIZATION &&//
//********************//

// set window properties
SetWindowTitle( "Everlasting Summer : Photographer" )
SetWindowSize( SCREEN_WIDTH, SCREEN_HEIGHT, 0 )
//SetAntialiasMode(1)
SetGenerateMipmaps(1)

// set display properties

SetVirtualResolution( SCREEN_WIDTH, SCREEN_HEIGHT )
SetOrientationAllowed( 0, 0, 1, 1 )

//Load ambient music

//activeAmbientMusic = LoadMusic(  "/media/sound/ambient/lake_shore_evening.mp3")
//SetMusicFileVolume( activeAmbientMusic, 25 )
//PlayMusic( activeAmbientMusic, PLAY_REPEAT )

//Load normal music

activeMusic = LoadMusic("/media/music/Everlasting_Summer.mp3")
SetMusicFileVolume( activeMusic, 100 )
//SetMusicSystemVolume( 100 )
//PlayMusic( activeMusic, PLAY_REPEAT )

CreateText( LOADING_TEXT_ID, "Loading..." )
SetTextPosition( LOADING_TEXT_ID, SCREEN_WIDTH/2 - GetTextTotalWidth(LOADING_TEXT_ID), SCREEN_HEIGHT/2)
SetTextSize(LOADING_TEXT_ID, 22)

CreateText( LOADING_STAGE_TEXT_ID, "Loading ui buttons" )
SetTextPosition( LOADING_STAGE_TEXT_ID, SCREEN_WIDTH/2 - GetTextTotalWidth(LOADING_STAGE_TEXT_ID), SCREEN_HEIGHT/2 + 50)
SetTextSize(LOADING_STAGE_TEXT_ID, 22)
SetTextColor(LOADING_STAGE_TEXT_ID, 175, 175, 120, 255)

Sync()

//Load UI

LoadImage(UI_ZOOM_IN, "/media/ui/zoom-in.png")
LoadImage(UI_ZOOM_OUT, "/media/ui/zoom-out.png")

global zoom_in_button
zoom_in_button = CreateSprite(UI_ZOOM_IN)
global zoom_in_button_hit = FALSE

global zoom_out_button
zoom_out_button = CreateSprite(UI_ZOOM_OUT)
global zoom_out_button_hit = FALSE

SetSpriteDepth( zoom_in_button, UI_DEPTH )
SetSpriteDepth( zoom_out_button, UI_DEPTH )

SetSpriteVisible( zoom_in_button, FALSE )
SetSpriteVisible( zoom_out_button, FALSE )


global buttonImage
LoadImage( BUTTON_NORMAL_IMAGE, "/media/ui/packbasic/PNG/grey_button08.png")

//Load outfit ui

Dim SlavyaPoses[MAX_SLAVYA_POSES] as ModelPose


//Init poses

ShowLoadStage("Outfit thumbnails" )

LoadImage( OUTFIT_P1_UI_BARE_IMG, "/media/ui/thumbnails/models/slavya/body/sl_1_body.png")
LoadImage( OUTFIT_P1_UI_DRESS_IMG, "/media/ui/thumbnails/models/slavya/outfit/dress/sl_1_dress.png")
LoadImage( OUTFIT_P1_UI_SWIM_IMG, "/media/ui/thumbnails/models/slavya/outfit/swim/sl_1_swim.png")
LoadImage( OUTFIT_P1_UI_PIONEER_IMG, "/media/ui/thumbnails/models/slavya/outfit/pioneer/sl_1_pioneer.png")
LoadImage( OUTFIT_P1_UI_SPORT_IMG, "/media/ui/thumbnails/models/slavya/outfit/sport/sl_1_sport.png")

ShowLoadStage("Emotions thumbnails" )
LoadImage( SL_P1_EMOTION_NORMAL, "/media/ui/thumbnails/models/slavya/emotions/sl_1_em_normal.png")
LoadImage( SL_P1_EMOTION_SERIOUS, "/media/ui/thumbnails/models/slavya/emotions/sl_1_em_serious.png")
LoadImage( SL_P1_EMOTION_SMILE, "/media/ui/thumbnails/models/slavya/emotions/sl_1_em_smile.png")


SlavyaPoses[1].active = POSE_ACTIVE
SlavyaPoses[1].id = SLAVYA_POSE_1

SlavyaPoses[1].outfitUI.bareButton = SetupOutfitSpriteButton( OUTFIT_P1_UI_BARE_IMG, OUTFIT_UI_POS_X, SCREEN_HEIGHT - OUTFIT_UI_SHIFT_Y )
SlavyaPoses[1].outfitUI.dressButton = SetupSmallOutfitSpriteButton( OUTFIT_P1_UI_DRESS_IMG, OUTFIT_UI_POS_X + OUTFIT_SHIFT_X + 70, SCREEN_HEIGHT - OUTFIT_UI_SHIFT_Y )
SlavyaPoses[1].outfitUI.swimButton = SetupSmallOutfitSpriteButton( OUTFIT_P1_UI_SWIM_IMG, OUTFIT_UI_POS_X + OUTFIT_SHIFT_X + 70*2, SCREEN_HEIGHT - OUTFIT_UI_SHIFT_Y )
SlavyaPoses[1].outfitUI.pioneerButton = SetupOutfitSpriteButton( OUTFIT_P1_UI_PIONEER_IMG, OUTFIT_UI_POS_X + OUTFIT_SHIFT_X + 70*3, SCREEN_HEIGHT - OUTFIT_UI_SHIFT_Y )
SlavyaPoses[1].outfitUI.sportButton = SetupSmallOutfitSpriteButton( OUTFIT_P1_UI_SPORT_IMG, OUTFIT_UI_POS_X + OUTFIT_SHIFT_X + 70*4 + 10, SCREEN_HEIGHT - OUTFIT_UI_SHIFT_Y )

SlavyaPoses[1].lastEmotion = 3
SlavyaPoses[1].emotionUI[1].caption = "Normal"
SlavyaPoses[1].emotionUI[1].emotionButton = SetupEmotionSpriteButton( SL_P1_EMOTION_NORMAL, EMOTION_UI_POS_X, EMOTION_UI_POS_Y )
SlavyaPoses[1].emotionUI[1].backButton = SetupButton( EM_NORMAL_BUTTON, BUTTON_NORMAL_IMAGE, EMOTION_UI_POS_X, EMOTION_UI_POS_Y, 1.2, 1.2 )

SlavyaPoses[1].emotionUI[2].caption = "Serious"
SlavyaPoses[1].emotionUI[2].emotionButton = SetupEmotionSpriteButton( SL_P1_EMOTION_SERIOUS, EMOTION_UI_POS_X + 70, EMOTION_UI_POS_Y )
SlavyaPoses[1].emotionUI[2].backButton = SetupButton( EM_NORMAL_BUTTON + 2, BUTTON_NORMAL_IMAGE, EMOTION_UI_POS_X + 70, EMOTION_UI_POS_Y, 1.2, 1.2 )

SlavyaPoses[1].emotionUI[3].caption = "Smile"
SlavyaPoses[1].emotionUI[3].emotionButton = SetupEmotionSpriteButton( SL_P1_EMOTION_SMILE, EMOTION_UI_POS_X + 140, EMOTION_UI_POS_Y )
SlavyaPoses[1].emotionUI[3].backButton = SetupButton( EM_NORMAL_BUTTON + 3, BUTTON_NORMAL_IMAGE, EMOTION_UI_POS_X + 140, EMOTION_UI_POS_Y, 1.2, 1.2 )


SlavyaPoses[2].active = POSE_INACTIVE
SlavyaPoses[3].active = POSE_INACTIVE
SlavyaPoses[4].active = POSE_INACTIVE



// UI Button setup //
button as UIButton

for i = 1 to MAX_SLAVYA_POSES 
	if( SlavyaPoses[i].active = POSE_INACTIVE )
		continue
	endif	
	
	SetUISpriteButton( SlavyaPoses[i].outfitUI.bareButton )
	SetUISpriteButton( SlavyaPoses[i].outfitUI.dressButton )
	SetUISpriteButton( SlavyaPoses[i].outfitUI.swimButton )
	SetUISpriteButton( SlavyaPoses[i].outfitUI.pioneerButton )
	SetUISpriteButton( SlavyaPoses[i].outfitUI.sportButton )
	
	for j = 1 to SlavyaPoses[i].lastEmotion 
		SetUISpriteButton( SlavyaPoses[i].emotionUI[j].backButton )
		SetUISpriteButton( SlavyaPoses[i].emotionUI[j].emotionButton )
		
		SetSpriteDepth( SlavyaPoses[i].emotionUI[j].backButton.sprite, 100 )
	next j
next i

global activePoseId  = 1
global activePose as ModelPose

activePose = SlavyaPoses[ activePoseId ]



//Load Slavya body/emotions/outfit sprites

ShowLoadStage("Sprites for Slavya...")

LoadImage(SLAVYA_BODY_1, "/media/models/slavya/body/sl_1_body.png")

ShowLoadStage("Sprites for Slavya - Emotions...")
LoadImage(SLAVYA_EM_SMILE, "/media/models/slavya/emotions/sl_1_em_smile.png")

ShowLoadStage("Sprites for Slavya - Outfit (Dress)...")
LoadImage(slavya_outfit_dress_DRESS_1, "/media/models/slavya/outfit/dress/sl_1_dress.png")
ShowLoadStage("Sprites for Slavya - Outfit (Pioneer)...")
LoadImage(SLAVYA_OUTFIT_SWIM_1, "/media/models/slavya/outfit/swim/sl_1_swim.png")
ShowLoadStage("Sprites for Slavya - Outfit (Swim)...")
LoadImage(SLAVYA_OUTFIT_PIONEER_1, "/media/models/slavya/outfit/pioneer/sl_1_pioneer.png")
ShowLoadStage("Sprites for Slavya - Outfit (Sport)...")
LoadImage(SLAVYA_OUTFIT_SPORT_1, "/media/models/slavya/outfit/sport/sl_1_sport.png")

SetImageMinFilter(SLAVYA_BODY_1, 1)
SetImageMagFilter(SLAVYA_BODY_1, 1)

SetImageMinFilter(SLAVYA_EM_SMILE, 1)
SetImageMagFilter(SLAVYA_EM_SMILE, 1)


global model_scale_x# = start_scale_x#
global model_scale_y# = start_scale_y#

global slavya_body
slavya_body = CreateSprite(SLAVYA_BODY_1)
SetSpriteDepth( slavya_body, GIRL_BODY_DEPTH )

global slavya_emotions
slavya_emotions = CreateSprite(SLAVYA_EM_SMILE)
SetSpriteDepth( slavya_emotions, GIRL_EMOTION_DEPTH )


global slavya_outfit_dress
slavya_outfit_dress = CreateSprite(slavya_outfit_dress_DRESS_1)
SetSpriteDepth( slavya_outfit_dress, GIRL_OUTFIT_DEPTH )

global slavya_outfit_swim
slavya_outfit_swim = CreateSprite(slavya_outfit_swim_1)
SetSpriteDepth( slavya_outfit_swim, GIRL_OUTFIT_DEPTH )
SetSpriteVisible( slavya_outfit_swim, FALSE )

global slavya_outfit_pioneer
slavya_outfit_pioneer = CreateSprite(SLAVYA_OUTFIT_PIONEER_1)
SetSpriteDepth( slavya_outfit_pioneer, GIRL_OUTFIT_DEPTH )
SetSpriteVisible( slavya_outfit_pioneer, FALSE )

global slavya_outfit_sport
slavya_outfit_sport = CreateSprite(SLAVYA_OUTFIT_SPORT_1 )
SetSpriteDepth( slavya_outfit_sport, GIRL_OUTFIT_DEPTH )
SetSpriteVisible( slavya_outfit_sport, FALSE )


global body_offset_x = 600
global body_offset_y = 10

//Girl
ZoomSlavya( model_scale_x#, model_scale_y# )
SetSlavyaPosition( body_offset_x, body_offset_y, model_scale_x#, model_scale_y# )

//================//
//Load backgrounds//

ShowLoadStage( "Loading background..." )

LoadImage( BACKGROUND_BEACH_SUNSET, "/media/backgrounds/ext_beach_sunset.jpg")

backgrounds = CreateSprite( BACKGROUND_BEACH_SUNSET )

SetSpriteDepth( backgrounds, BACKGROUND_DEPTH )


SetSpriteScale( backgrounds, BACKGROUND_SCALE_X, BACKGROUND_SCALE_Y )

//UI

SetSpritePosition( zoom_in_button, GetVirtualWidth() - 256, 12 )
SetSpritePosition( zoom_out_button, GetVirtualWidth() - 128, 12 )

//Background
SetSpritePosition( backgrounds, 0, 0 )



// Main vars
global oldx = -1
global oldy = -1
global old_ui_sprite = 0

SetTextVisible( LOADING_TEXT_ID, FALSE )
SetTextVisible( LOADING_STAGE_TEXT_ID, FALSE)



// && MAIN VARS && //

#constant FALSE = 0
#constant TRUE = 1

ui_button_hit = FALSE

bareButtonHit = FALSE
dressButtonHit = FALSE
swimButtonHit = FALSE
pioneerButtonHit = FALSE
sportButtonHit = FALSE

zoom_in_button_hit = FALSE
zoom_out_button_hit = FALSE

draggingModel = FALSE

SetSpriteVisible( zoom_in_button, TRUE )
SetSpriteVisible( zoom_out_button, TRUE )

//&&&&&&&&&&&&&&&&&&&&&&&&&&&
// MAIN LOOP
//&&&&&&&&&&&&&&&&&&&&&&&&&&&
do

	if( not draggingModel )
		//Check ui interaction
		ui_hit = GetSpriteHit(GetPointerX(), GetPointerY())

		HighlightZoomButtons( ui_hit )

		HighlightBareButton( ui_hit )
		HighlightDressButton( ui_hit )
		HighlightSwimButton( ui_hit )
		HighlightPioneerButton( ui_hit )
		HighlightSportButton( ui_hit )
		
		if( GetPointerState() = 1 ) //pressed
			ui_button_hit = FALSE
			
			if( ui_hit = activePose.outfitUI.bareButton.sprite )
				SetSpriteColorRed( activePose.outfitUI.bareButton.sprite, 70 )
				bareButtonHit = true
				ui_button_hit = TRUE
			else
				SetSpriteColorRed( activePose.outfitUI.bareButton.sprite, 255 )
			endif

			if( ui_hit = activePose.outfitUI.dressButton.sprite )
				SetSpriteColorRed( activePose.outfitUI.dressButton.sprite, 70 )
				dressButtonHit = true
				ui_button_hit = TRUE
			else
				SetSpriteColorRed( activePose.outfitUI.dressButton.sprite, 255 )
			endif

			if( ui_hit = activePose.outfitUI.swimButton.sprite )
				SetSpriteColorRed( activePose.outfitUI.swimButton.sprite, 70 )
				swimButtonHit = true
				ui_button_hit = TRUE
			else
				SetSpriteColorRed( activePose.outfitUI.swimButton.sprite, 255 )
			endif

			if( ui_hit = activePose.outfitUI.pioneerButton.sprite )
				SetSpriteColorRed( activePose.outfitUI.pioneerButton.sprite, 70 )
				pioneerButtonHit = true
				ui_button_hit = TRUE
			else
				SetSpriteColorRed( activePose.outfitUI.pioneerButton.sprite, 255 )
			endif

			if( ui_hit = activePose.outfitUI.sportButton.sprite )
				SetSpriteColorRed( activePose.outfitUI.sportButton.sprite, 70 )
				sportButtonHit = true
				ui_button_hit = TRUE
			else
				SetSpriteColorRed( activePose.outfitUI.sportButton.sprite, 255 )
			endif
		endif

		old_ui_sprite = ui_hit	
	endif
    
    //Check girl interaction
    
    if( GetPointerReleased() = 1 ) //clear highlight press on button up
		oldx = -1
		oldy = -1
		SetSpriteColorRed( zoom_in_button, 255 )
		SetSpriteColorRed( zoom_out_button, 255 )
		
		SetSpriteColorRed( activePose.outfitUI.bareButton.sprite, 255 )
		SetSpriteColorRed( activePose.outfitUI.dressButton.sprite, 255 )
		SetSpriteColorRed( activePose.outfitUI.swimButton.sprite, 255 )
		SetSpriteColorRed( activePose.outfitUI.pioneerButton.sprite, 255 )
		SetSpriteColorRed( activePose.outfitUI.sportButton.sprite, 255 )

		SetSpriteColorBlue( slavya_body, 255 )
		SetSpriteColorBlue( slavya_outfit_dress, 255 )
		SetSpriteColorBlue( slavya_outfit_swim, 255 )
		SetSpriteColorBlue( slavya_outfit_pioneer, 255 )
		SetSpriteColorBlue( slavya_outfit_sport, 255 )
		
		if( bareButtonHit )
			SetSpriteVisible( slavya_outfit_dress, FALSE )
			SetSpriteVisible( slavya_outfit_swim, FALSE )
			SetSpriteVisible( slavya_outfit_pioneer, FALSE )
			SetSpriteVisible( slavya_outfit_sport, FALSE )
		endif

		if( dressButtonHit )
			SetSpriteVisible( slavya_outfit_swim, FALSE )
			SetSpriteVisible( slavya_outfit_dress, TRUE )
			SetSpriteVisible( slavya_outfit_pioneer, FALSE )
			SetSpriteVisible( slavya_outfit_sport, FALSE )
		endif

		if( swimButtonHit )
			SetSpriteVisible( slavya_outfit_dress, FALSE )
			SetSpriteVisible( slavya_outfit_swim, TRUE )
			SetSpriteVisible( slavya_outfit_pioneer, FALSE )
			SetSpriteVisible( slavya_outfit_sport, FALSE )
		endif

		if( pioneerButtonHit )
			SetSpriteVisible( slavya_outfit_dress, FALSE )
			SetSpriteVisible( slavya_outfit_swim, FALSE )
			SetSpriteVisible( slavya_outfit_pioneer, TRUE )
			SetSpriteVisible( slavya_outfit_sport, FALSE )
		endif

		if( sportButtonHit )
			SetSpriteVisible( slavya_outfit_dress, FALSE )
			SetSpriteVisible( slavya_outfit_swim, FALSE )
			SetSpriteVisible( slavya_outfit_pioneer, FALSE )
			SetSpriteVisible( slavya_outfit_sport, TRUE )
		endif
		
	    bareButtonHit = FALSE
	    dressButtonHit = FALSE
	    swimButtonHit = FALSE
	    pioneerButtonHit = FALSE
	    sportButtonHit = FALSE
	    
	    ui_button_hit = FALSE

	    draggingModel = FALSE
	endif
    
	if( GetPointerState() = 1 )	
		
		if( not ui_button_hit = TRUE and not IsZoomButtonHit() and oldx > 0 and oldy > 0 )
			dx = GetPointerX() - oldx
			dy = GetPointerY() - oldy

			sprite_hit = GetSpriteHit(GetPointerX(), GetPointerY())			
			
			if( IsGirlSpriteHit( sprite_hit ) or draggingModel )
				
				body_offset_x = body_offset_x +  dx
				body_offset_y = body_offset_y +  dy
				
				draggingModel = TRUE
				
				SetSlavyaPosition( body_offset_x, body_offset_y, model_scale_x#, model_scale_y# )
				
				SetSpriteColorBlue( slavya_body, 10 )
				SetSpriteColorBlue( slavya_outfit_dress, 10 )
				SetSpriteColorBlue( slavya_outfit_swim, 10 )
				SetSpriteColorBlue( slavya_outfit_pioneer, 10 )
			endif
		endif
		
		oldx = GetPointerX()
		oldy = GetPointerY()
		
	endif

    
    Sync()
loop
