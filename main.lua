application:setWindowSize(1024, 600)

local background = Bitmap.new(Texture.new("ext_beach_sunset.jpg"))

stage:addChild( background )

local slavya_body = Bitmap.new(Texture.new("sl_1_body.png"))

slavya_body:setScale(0.2,0.2,0.2)

stage:addChild( slavya_body )
